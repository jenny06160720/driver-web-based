var express = require('express');
var router = express.Router();

var https = require('https');
let url = "https://api.data.gov.hk/v1/carpark-info-vacancy?data=vacancy&lang=en_US&carparkIds=";
let json

//GET BACK ALL THE POST
router.get('/:id', async (req, res) => {
    try{
        let urls = url + req.params['id'];
        https.get(urls,(res) => {
            let body = "";
            res.on("data", (chunk) => {
                body += chunk;
            });
        
            res.on("end", () => {
                try {
                    json = JSON.parse(body);
                } catch (error) {
                    console.error(error.message);
                };
            });
        
        }).on("error", (error) => {
            console.error(error.message);
        });

        res.json(json);
    }catch(err){
        res.json({message: err});
    }
});

module.exports = router;

