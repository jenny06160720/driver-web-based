var express = require('express');
var router = express.Router();

const modleOfData = require('../models/ToRecord');

const axios = require("axios");
const toRecord = require("../db/torecordsOfDriverDB");
const driverdb = require("../db/driver");

const driver = new driverdb();
const tos = new toRecord();


(async () => {
    await driver.connect();
    tos.connect(driver);
})();

// GET ALL DATA
router.get("/", async (req, res) => {
    try {
        res.send(await tos.getAll());
    } catch (err) {
        res.json({ message: err });
    }
});

// SPECIFIC GET    
router.get('/:toID', async (req, res) => {
    try {
        res.send(await tos.getByID(req.params.toID));
    } catch (err) {
        res.json({ message: err });
    }
});

// Delete BY ID
router.delete('/:toID', async (req, res) => {
    try {
        res.send(await tos.deleteByID(req.params.toID));
    } catch (err) {
        res.json({ message: err });
    }
});

//SUMBITS A POST
router.post('/', async (req, res) => {
    const datas = new modleOfData({
        // 1
        To: req.body.To
    });
    try {
        res.send(await tos.createRecord(datas));
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
