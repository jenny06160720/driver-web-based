var express = require('express');
var router = express.Router();

const modleOfData = require('../models/Favourites');
const axios = require("axios");
const Favourite = require("../db/favouritesOfDriverDB");
const driverdb = require("../db/driver");

const Post = new Favourite();
const driver = new driverdb();

(async () => {
    await driver.connect();
    Post.connect(driver);
})();

// GET ALL DATA
router.get("/", async (req, res) => {
    try {
        res.send(await Post.getAll());
    } catch (err) {
        res.json({ message: err });
    }
});

// SPECIFIC GET
router.get('/:favouriteID', async (req, res) => {
    try {
        res.send(await Post.getByID(req.params.favouriteID));
    } catch (err) {
        res.json({ message: err });
    }
});

//Delete BY ID
router.delete('/:favouriteID', async (req, res) => {
    try {
        res.send(await Post.deleteByID(req.params.favouriteID));
    } catch (err) {
        res.json({ message: err });
    }
});

//SUMBITS A POST
router.post('/', async (req, res) => {
    const datas = new modleOfData({
        // 1
        Favourite: req.body.Favourite
    });
    try {
        res.send(await Post.createRecord(datas));
    } catch (err) {
        res.json({ message: err });
    }
});

module.exports = router;
