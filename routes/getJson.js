var express = require('express');
var router = express.Router();

var https = require('https');
let url = "https://api.data.gov.hk/v1/carpark-info-vacancy?lang=en_US";
let json

https.get(url,(res) => {
    let body = "";
    res.on("data", (chunk) => {
        body += chunk;
    });

    res.on("end", () => {
        try {
            json = JSON.parse(body);
            // do something with JSON
            // console.log(json);
        } catch (error) {
            console.error(error.message);
        };
    });

}).on("error", (error) => {
    console.error(error.message);
});

//GET BACK ALL THE POST
router.get('/', async (req, res) => {
    try{
         res.json(json);
    }catch(err){
        res.json({message: err});
    }
});

module.exports = router;

