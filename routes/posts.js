var express = require('express');
var router = express.Router();

const modleOfData = require('../models/Posts');
const axios = require("axios");
const posts = require("../db/postsOfDriverDB");
const driverdb = require("../db/driver");

const Post = new posts();
const driver = new driverdb();

(async () => {
    await driver.connect();
    Post.connect(driver);
})();


// GET ALL DATA
router.get("/", async (req, res) => {
    try {
        res.send(await Post.getAll());
    } catch (err) {
        res.json({ message: err });
    }
});

// SPECIFIC GET    
router.get('/:postId', async (req, res) => {
    try {
        res.send(await Post.getByID(req.params.postId));
    } catch (err) {
        res.json({ message: err });
    }
});

// Delete Post
router.delete('/:postId', async (req, res) => {
    try {
        res.send(await Post.deleteByID(req.params.postId));
    } catch (err) {
        res.json({ message: err });
    }
});

//SUMBITS A POST
router.post('/', async (req, res) => {
    const datas = new modleOfData({
        // 3
        License_Plate_Number: req.body.License_Plate_Number,
        // 4
        Type_Of_License_Plate: req.body.Type_Of_License_Plate,
        // 5
        Fee_Of_Vehicle_License: req.body.Fee_Of_Vehicle_License,
        // 6
        Expiry_Date_Of_Vehicle_License: req.body.Expiry_Date_Of_Vehicle_License,
        // 7
        Reminder_Date_Of_Vehicle_License: req.body.Reminder_Date_Of_Vehicle_License,
        // 8
        Vehicle_Insurance_Number: req.body.Vehicle_Insurance_Number,
        // 9
        Expiry_Date_Of_Vehicle_Insurance: req.body.Expiry_Date_Of_Vehicle_Insurance,
        // 10
        Reminder_Date_Of_Vehicle_Insurance: req.body.Reminder_Date_Of_Vehicle_Insurance,
        // 11
        Date_Of_Engine_Oil_Change: req.body.Date_Of_Engine_Oil_Change,
        // 12
        Reminder_Date_Of_Engine_Oil_Change: req.body.Reminder_Date_Of_Engine_Oil_Change,
        // 13
        Date_Of_Gearbox_Oil_Change: req.body.Date_Of_Gearbox_Oil_Change,
        // 14
        Reminder_Date_Of_Gearbox_Oil_Change: req.body.Reminder_Date_Of_Gearbox_Oil_Change
    });
    try {
        res.send(await Post.createRecord(datas));
    } catch (err) {
        res.json({ message: err });
    }
});

//Updata a post
router.patch('/:postId', async (req, res) => {
    var updateValue = { $set: {
        // 2
        Update_Time: req.body.Update_Time,
        // 3
        License_Plate_Number: req.body.License_Plate_Number,
        // 4
        Type_Of_License_Plate: req.body.Type_Of_License_Plate,
        // 5
        Fee_Of_Vehicle_License: req.body.Fee_Of_Vehicle_License,
        // 6
        Expiry_Date_Of_Vehicle_License: req.body.Expiry_Date_Of_Vehicle_License,
        // 7
        Reminder_Date_Of_Vehicle_License: req.body.Reminder_Date_Of_Vehicle_License,
        // 8
        Vehicle_Insurance_Number: req.body.Vehicle_Insurance_Number,
        // 9
        Expiry_Date_Of_Vehicle_Insurance: req.body.Expiry_Date_Of_Vehicle_Insurance,
        // 10
        Reminder_Date_Of_Vehicle_Insurance: req.body.Reminder_Date_Of_Vehicle_Insurance,
        // 11
        Date_Of_Engine_Oil_Change: req.body.Date_Of_Engine_Oil_Change,
        // 12
        Reminder_Date_Of_Engine_Oil_Change: req.body.Reminder_Date_Of_Engine_Oil_Change,
        // 13
        Date_Of_Gearbox_Oil_Change: req.body.Date_Of_Gearbox_Oil_Change,
        // 14
        Reminder_Date_Of_Gearbox_Oil_Change: req.body.Reminder_Date_Of_Gearbox_Oil_Change
    }};

    try {
        res.send(await Post.UpdateRecord(req.params.postId,updateValue));
    } catch (err) {
        res.json({ message: err });
    }
})

module.exports = router;
