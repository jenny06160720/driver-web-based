const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    Create_Time: { 
        // 1
        type: Date,
        default: Date.now
    },
    Update_Time: { 
        // 2
        type: Date,
        default: Date.now
    },
    License_Plate_Number:{
        // 3
        type: String,
        require: true
    },
    Type_Of_License_Plate:{
        // 4
        type: String,
        require: true
    },
    Fee_Of_Vehicle_License:{
        // 5
        type: String,
        require: true
    },
    Expiry_Date_Of_Vehicle_License:{
        // 6
        type: Date,
        require: true
    },
    Reminder_Date_Of_Vehicle_License:{
        // 7
        type: Date,
        require: true
    },
    Vehicle_Insurance_Number:{
        // 8
        type: String,
        require: true
    },
    Expiry_Date_Of_Vehicle_Insurance:{
        // 9
        type: Date,
        require: true
    },
    Reminder_Date_Of_Vehicle_Insurance:{
        // 10
        type: Date,
        require: true
    },
    Date_Of_Engine_Oil_Change:{
        // 11
        type: Date,
        require: true
    },
    Reminder_Date_Of_Engine_Oil_Change:{
        // 12
        type: Date,
        require: true
    },
    Date_Of_Gearbox_Oil_Change:{
        // 13
        type: Date,
        require: true
    },
    Reminder_Date_Of_Gearbox_Oil_Change:{
        // 14
        type: Date,
        require: true
    }

});


module.exports = mongoose.model('Posts', PostSchema);
