const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    Favourite:{
        // 1
        type: String,
        require: true
    }
});

module.exports = mongoose.model('Favourite', PostSchema);
