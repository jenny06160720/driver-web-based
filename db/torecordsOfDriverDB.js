var Connect = require("./connect");
var mongoose = require('mongoose');

class toRecord extends Connect {
    constructor() {
        super("DriverDB", "torecords");
    }

    async getByID(_id) {
        return await this.collection.find({"_id": mongoose.Types.ObjectId(_id)}).toArray();
    }

    async deleteByID(_id) {
        return await this.collection.deleteOne({"_id": mongoose.Types.ObjectId(_id)});
    }

    async createRecord(datas){
        return await this.collection.insertOne(datas);
    }
}

module.exports = toRecord;
