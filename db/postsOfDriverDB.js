var Connect = require("./connect");
var mongoose = require('mongoose');

class postsDB extends Connect {
    constructor() {
        super("DriverDB", "posts");
    }

    async getByID(id) {
        return await this.collection.find({"_id":mongoose.Types.ObjectId(id)}).toArray();
    }

    async deleteByID(id) {
        return await this.collection.deleteOne({"_id": mongoose.Types.ObjectId(id)});
    }

    async createRecord(datas){
        return await this.collection.insertOne(datas);
    }

    async UpdateRecord(id,datas){
        return await this.collection.updateOne({"_id": mongoose.Types.ObjectId(id)},datas);
    }
}

module.exports = postsDB;
