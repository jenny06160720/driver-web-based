const { MongoClient } = require("mongodb");

class driver {
    #drivers;
    async connect() {
        this.#drivers = new MongoClient("mongodb+srv://Jenny:!Jl68797035@driverdb.imnkf.mongodb.net/DriverDB?retryWrites=true&w=majority", { useUnifiedTopology: true });
        await this.#drivers.connect();
    }

    get drivers() {
        return this.#drivers;
    }

    get db() {
        return this.#drivers.db();
    }

    db(name) {
        return this.#drivers.db(name)
    }
}

module.exports = driver;