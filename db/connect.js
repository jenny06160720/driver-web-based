class connect {
    collection;

    constructor(dbName, table) {
        this.dbName = dbName;
        this.table = table;
    }

    connect(driver) {
        this.collection = driver.db(this.dbName).collection(this.table);
    }

    async getAll() {        
        return await this.collection.find().toArray();
    }
}

module.exports = connect;
