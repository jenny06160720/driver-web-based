var Connect = require("./connect");
var mongoose = require('mongoose');

class Favourite extends Connect {
    constructor() {
        super("DriverDB", "favourites");
    }

    async getByID(id) {
        return await this.collection.find({"_id": mongoose.Types.ObjectId(id)}).toArray();
    }

    async deleteByID(id) {
        return await this.collection.deleteOne({"_id": mongoose.Types.ObjectId(id)});
    }

    async createRecord(datas){
        return await this.collection.insertOne(datas);
    }
}

module.exports = Favourite;
