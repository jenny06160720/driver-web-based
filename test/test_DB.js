const { expect } = require("chai");
const should = require("should");
const driver = require("../db/driver");
const Favourite_DB = require("../db/favouritesOfDriverDB");
const Posts_DB = require("../db/postsOfDriverDB");
const To_Record_DB = require("../db/torecordsOfDriverDB");


describe("Test method of Mongodb - driverDB", async () => {
    const Driver = new driver();
    before("Kill Timeout", async () => {
        await new Promise(resolve => setTimeout(resolve, 2000));
    });

    before(async () => {
        await Driver.connect();
    })

    it("Connect to Mongodb - driverDB", function () {
        should.exist(Driver.db);
    });

    describe("Test method of Favourite_DB in Mongodb - driverDB", async () => {
        const Favourite_Table_Test = new Favourite_DB();

        // Connect the favourite table in driverDB

        before(function () {
            Favourite_Table_Test.connect(Driver);
            Favourite_Cloud_Data = Driver.db("DriverDB").collection("favourites");
        });

        it("Get All Records in Favourite_DB", async () => {
            const favourite_record = await Favourite_Table_Test.getAll();
            const expected_count = await Driver.db("DriverDB").collection("favourites").countDocuments();
            favourite_record.length.should.equal(expected_count);
        });

        it("Get By ID - Record in Favourite_DB", async () => {
            const data_Of_Favourite_id = "5ff73532f33f061e4cb89671";
            const  record_Of_Favourite = await Favourite_Table_Test.getByID(data_Of_Favourite_id);
            const expected_count = await Favourite_Cloud_Data.countDocuments({_id: data_Of_Favourite_id});
            record_Of_Favourite.length.should.equal(expected_count);
        });

        it("Get By ID - No Record in Favourite_DB", async () => {
            const data_Of_Favourite = "AAAAAAAAAAAAAAAAAAAAAAAA";
            const record_Of_Favourite = await Favourite_Table_Test.getByID(data_Of_Favourite);
            record_Of_Favourite.length.should.equal(0);
        });

        it("Delete By ID - Record in Favourite_DB", async () => {
            const id = "5ff73532f33f061e4cb89611";
            const record_Of_Favourite = await Favourite_Table_Test.deleteByID(id);
            record_Of_Favourite["ok"] == 1;
        });
        
        it("Delete By ID - No Record in Favourite_DB", async () => {
            const id = "5ff73532f33f061e4cb89611";
            const record_Of_Favourite = await Favourite_Table_Test.deleteByID(id);
            record_Of_Favourite["message"] == {};
        });

        it("Create Record in Favourite_DB", async () => {
            const datas = {
                "Favourite": "1"
            }

            const record_Of_Favourite = await Favourite_Table_Test.createRecord(datas);
            record_Of_Favourite["ok"] == 1;
        });
    });

    describe("Test method of ToRecordDB in Mongodb - driverDB", async () => {
        const ToRecord_Table_Test = new To_Record_DB();
        // Connect the torecords table in driverDB

        before(function () {
            ToRecord_Table_Test.connect(Driver);
            To_Cloud_DB = Driver.db("DriverDB").collection("torecords");
        });

        it("Get All Records in ToRecordDB", async () => {
            const to_records = await ToRecord_Table_Test.getAll();
            const expected_count = await Driver.db("DriverDB").collection("torecords").countDocuments();
            to_records.length.should.equal(expected_count);
        });

        it("Get By ID - Record in ToRecordDB", async () => {
            const id = "5ff73532f33f061e4cb89672";
            const record_Of_To = await ToRecord_Table_Test.getByID(id);
            const expected_count = await To_Cloud_DB.countDocuments({_id: id});
            record_Of_To.length.should.equal(expected_count);
        });

        it("Get By ID - No Record in ToRecordDB", async () => {
            const id = "CCFCCFCCFCCFCCFCCFCCFCCF";
            const record_Of_To = await ToRecord_Table_Test.getByID(id);
            record_Of_To.length.should.equal(0);
        });

        it("Delete By ID - Record in ToRecordDB", async () => {
            const id = "5ff73532f33f061e4cb89611";
            const record_Of_To = await ToRecord_Table_Test.deleteByID(id);
            record_Of_To["ok"] == 1;
        });
        
        it("Delete By ID - No Record in ToRecordDB", async () => {
            const id = "5ff73532f33f061e4cb89611";
            const record_Of_To = await ToRecord_Table_Test.deleteByID(id);
            record_Of_To["message"] == {};
        });

        it("Create Record in ToRecordDB", async () => {
            const datas = {
                "To": "1"
            }

            const record_Of_To = await ToRecord_Table_Test.createRecord(datas);
            record_Of_To["ok"] == 1;
        });
    });

    describe("Test method of Favourite_DB in Mongodb - driverDB", async () => {
        const Posts_Table_Test = new Posts_DB();
        // Connect the posts table in driverDB

        before(function () {
            Posts_Table_Test.connect(Driver);
            To_Cloud_DB = Driver.db("DriverDB").collection("posts");
        });

        it("Get All Records in PostsDB", async () => {
            const posts_records = await Posts_Table_Test.getAll();
            const expected_count = await Driver.db("DriverDB").collection("posts").countDocuments();
            posts_records.length.should.equal(expected_count);
        });

        it("Get By ID - Record in PostsDB", async () => {
            const id = "5ff73532f33f061e4cb89673";
            const posts_records = await Posts_Table_Test.getByID(id);
            const expected_count = await To_Cloud_DB.countDocuments({_id: id});
            posts_records.length.should.equal(expected_count);
        });

        it("Get By ID - No Record in PostsDB", async () => {
            const id = "5ff73532f33f061e4cb89674";
            const posts_records = await Posts_Table_Test.getByID(id);
            posts_records.length.should.equal(0);
        });

        it("Delete By ID - Record in PostsDB", async () => {
            const id = "5ff73532f33f061e4cb89611";
            const posts_records = await Posts_Table_Test.deleteByID(id);
            posts_records["ok"] == 1;
        });
        
        it("Delete By ID - No Record in PostsDB", async () => {
            const id = "5ff73532f33f061e4cb89611";
            const posts_records = await Posts_Table_Test.deleteByID(id);
            posts_records["message"] == {};
        });

        it("Create Record in PostsDB", async () => {
            const datas = {
                "License_Plate_Number": "1",
                "Type_Of_License_Plate": "2",
                "Fee_Of_Vehicle_License": "3",
                "Expiry_Date_Of_Vehicle_License": "20200101",
                "Reminder_Date_Of_Vehicle_License": "20200102",
                "Vehicle_Insurance_Number": "4",
                "Expiry_Date_Of_Vehicle_Insurance": "20200103",
                "Reminder_Date_Of_Vehicle_Insurance": "20200104",
                "Date_Of_Engine_Oil_Change": "20200105",
                "Reminder_Date_Of_Engine_Oil_Change": "20200106",
                "Date_Of_Gearbox_Oil_Change": "20200107",
                "Reminder_Date_Of_Gearbox_Oil_Change": "20200108"
            }

            const posts_records = await Posts_Table_Test.createRecord(datas);
            posts_records["ok"] == 1;
        });

        it("Update Record - Record in PostsDB", async () => {
            const id = "5ff73532f33f061e4cb89611";
            const datas = { $set:{
                "License_Plate_Number": "1",
                "Type_Of_License_Plate": "2",
                "Fee_Of_Vehicle_License": "3",
                "Expiry_Date_Of_Vehicle_License": "20200101",
                "Reminder_Date_Of_Vehicle_License": "20200102",
                "Vehicle_Insurance_Number": "4",
                "Expiry_Date_Of_Vehicle_Insurance": "20200103",
                "Reminder_Date_Of_Vehicle_Insurance": "20200104",
                "Date_Of_Engine_Oil_Change": "20200105",
                "Reminder_Date_Of_Engine_Oil_Change": "20200106",
                "Date_Of_Gearbox_Oil_Change": "20200107",
                "Reminder_Date_Of_Gearbox_Oil_Change": "20200108",
                "Update_Time": "20200109"
            }}

            const posts_records = await Posts_Table_Test.UpdateRecord(id,datas);
            posts_records["ok"] == 1;
        });

        it("Update Record - No Record in PostsDB", async () => {
            const id = "5ff73532f33f061e4cb89611";
            const datas = { $set:{
                "License_Plate_Number": "1",
                "Type_Of_License_Plate": "2",
                "Fee_Of_Vehicle_License": "3",
                "Expiry_Date_Of_Vehicle_License": "20200101",
                "Reminder_Date_Of_Vehicle_License": "20200102",
                "Vehicle_Insurance_Number": "4",
                "Expiry_Date_Of_Vehicle_Insurance": "20200103",
                "Reminder_Date_Of_Vehicle_Insurance": "20200104",
                "Date_Of_Engine_Oil_Change": "20200105",
                "Reminder_Date_Of_Engine_Oil_Change": "20200106",
                "Date_Of_Gearbox_Oil_Change": "20200107",
                "Reminder_Date_Of_Gearbox_Oil_Change": "20200108",
                "Update_Time": "20200109"
            }}

            const posts_records = await Posts_Table_Test.UpdateRecord(id,datas);
            posts_records["message"] == {};
        });

    });
});