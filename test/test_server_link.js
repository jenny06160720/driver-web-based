const supertest = require("supertest");
const should = require("should");
const chai = require("chai");
const app = require("../index");
const { response } = require("express");
const { doesNotThrow } = require("should");
const express = require('express');
chai.should();
chai.use(require('chai-things'));

const request = supertest(app);
describe("Test Server Link", async () => {
    before("Kill Timeout", async () => {
        await new Promise(resolve => setTimeout(resolve, 2000));
    });

    describe("Test Link - posts", async () => {
        it("Test :/posts/ - Get", async () => {
            const data = (await request.get("/posts")).body;
        });

        it("Test :/posts/ - Get", async () => {
            request
                .post(`/posts/`)
                .send()
                .expect(201)
                .then((res) => {
                    res.body.all.should.have.property("_id");
                });
        });

        it("Test :/{postId} - Get By ID ", async () => {
            const expected_postid = "5ff73532f33f061e4cb89673";
            const data = (await request.get(`/posts/${expected_postid}`)).body;
        });

        it("Test :/posts/ - Get By ID", async () => {
            const expected_postid = "5ff73532f33f061e4cb89673";
            request
                .post(`/posts/${expected_postid}`)
                .send()
                .expect(201)
                .then((res) => {
                    res.body.all.should.have.property("_id");
                });
        });

        it("Test :/{postId} - Delete By ID", async () => {
            const expected_postid = "5ff73532f33f061e4cb89674";
            const data = (await request.delete(`/posts/${expected_postid}`)).body;
            should.exist(data);
        });

        it("Test :/posts/ - Create Record", async () => {
            const datas = {
                "License_Plate_Number": "1",
                "Type_Of_License_Plate": "2",
                "Fee_Of_Vehicle_License": "3",
                "Expiry_Date_Of_Vehicle_License": "20200101",
                "Reminder_Date_Of_Vehicle_License": "20200102",
                "Vehicle_Insurance_Number": "4",
                "Expiry_Date_Of_Vehicle_Insurance": "20200103",
                "Reminder_Date_Of_Vehicle_Insurance": "20200104",
                "Date_Of_Engine_Oil_Change": "20200105",
                "Reminder_Date_Of_Engine_Oil_Change": "20200106",
                "Date_Of_Gearbox_Oil_Change": "20200107",
                "Reminder_Date_Of_Gearbox_Oil_Change": "20200108",
            }

            request
                .post(`/posts/`)
                .send(datas)
                .expect(201)
                .then((res) => {
                    res.body.should.have.property("ok", 1);
                });
        });

        it("Test :/{postId} - Update Record", async () => {
            const expected_postid = "5ff73532f33f061e4cb89674";
            const datas = {
                "License_Plate_Number": "1",
                "Type_Of_License_Plate": "2",
                "Fee_Of_Vehicle_License": "3",
                "Expiry_Date_Of_Vehicle_License": "20200101",
                "Reminder_Date_Of_Vehicle_License": "20200102",
                "Vehicle_Insurance_Number": "4",
                "Expiry_Date_Of_Vehicle_Insurance": "20200103",
                "Reminder_Date_Of_Vehicle_Insurance": "20200104",
                "Date_Of_Engine_Oil_Change": "20200105",
                "Reminder_Date_Of_Engine_Oil_Change": "20200106",
                "Date_Of_Gearbox_Oil_Change": "20200107",
                "Reminder_Date_Of_Gearbox_Oil_Change": "20200108",
                "Update_Time": "20200109"
            }

            request
                .patch(`/posts/${expected_postid}`)
                .send(datas)
                .expect(201)
                .then((res) => {
                    res.body.should.have.property("_id", expected_postid);
                });
        });
    });

    describe("Test Link - favourites", async () => {
        it("Test :/favourites/ - Get", async () => {
            const data = (await request.get("/favourites")).body;
            data.length.should.above(0);
        });

        it("Test :/favourites/ - Get", async () => {
            request
                .post(`/favourites/`)
                .send()
                .expect(201)
                .then((res) => {
                    res.body.all.should.have.property("_id");
                });
        });

        it("Test :/favourites/{favouriteID} - Get By ID", async () => {
            const expected_favouriteid = "5ff73532f33f061e4cb89674";
            const data = (await request.get(`/favourites/${expected_favouriteid}`)).body;
            data.should.all.have.property("_id", expected_favouriteid);
        });

        it("Test :/favourites/ - Get By ID", async () => {
            const expected_postid = "5ff73532f33f061e4cb89673";
            request
                .post(`/favourites/${expected_postid}`)
                .send()
                .expect(201)
                .then((res) => {
                    res.body.all.should.have.property("_id");
                });
        });


        it("Test :/{favouriteID} - Delete By ID", async () => {
            const expected_favouriteid = "5ff73532f33f061e4cb89674";
            const data = (await request.delete(`/favourites/${expected_favouriteid}`)).body;
            should.exist(data);
        });

        it("Test :/favourites/ - Create Record", async () => {
            const datas = {
                "Favourite": "1",
            }
            request
                .post(`/favourites/`)
                .send(datas)
                .expect(201)
                .then((res) => {
                    res.body.should.have.property("ok", 1);
                });
        });
    });

    describe("Test Link - to", async () => {
        it("Test :/to/ - Get ", async () => {
            const data = (await request.get("/to")).body;
            data.length.should.above(0);
        });

        it("Test :/to/ - Get", async () => {
            request
                .post(`/to/`)
                .send()
                .expect(201)
                .then((res) => {
                    res.body.all.should.have.property("_id");
                });
        });

        it("Test :/to/{toID} - Get By ID ", async () => {
            const expected_toid = "5ff73532f33f061e4cb89675"
            const data = (await request.get(`/to/${expected_toid}`)).body;
            data.should.all.have.property("_id", expected_toid);
        });

        it("Test :/to/{toID} - Get By ID", async () => {
            const expected_postid = "5ff73532f33f061e4cb89673";
            request
                .post(`/to/${expected_postid}`)
                .send()
                .expect(201)
                .then((res) => {
                    res.body.all.should.have.property("_id");
                });
        });

        it("Test :/to/{toID} - Delete By ID", async () => {
            const expected_toid = "5ff73532f33f061e4cb89675"
            const data = (await request.delete(`/to/${expected_toid}`)).body;
            should.exist(data);
        });

        it("Test :/to/ - Create Record", async () => {
            const datas = {
                "To": "1",
            }
            request
                .post(`/to/`)
                .send(datas)
                .expect(201)
                .then((res) => {
                    res.body.should.have.property("ok", 1);
                });
        });
    });

    describe("Test Link - getjson", async () => {
        it("Test :/getjson/ - Get ", async () => {
            const data = (await request.get("/getjson")).body;
            should.exist(data);
        });

        it("Test :/getjson/ - Get", async () => {
            request
                .post(`/getjson/`)
                .send()
                .expect(201)
                .then((res) => {
                    res.body.all.should.have.property("id");
                });
        });
    });

    describe("Test Link - getjson_id", async () => {
        it("Test :/getjson_id/{id} - Get By ID", async () => {
            const carparkIds = "12"
            const data = (await request.get(`/getjson_id/${carparkIds}`)).body;
            should.exist(data);
        });

        it("Test :/getjson_id/{id} - Get By ID", async () => {
            const carparkIds = "12"
            request
                .post(`/getjson_id/${carparkIds}`)
                .send()
                .expect(201)
                .then((res) => {
                    res.body.should.have.property("id");
                });
        });
    });
});