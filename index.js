const express = require('express');
const bodyParser = require('body-parser');
const app = express();
require('dotenv/config');

// using the body parser
app.use(bodyParser.json());

//Import Routes
const postsRoute = require('./routes/posts');
const favouritesRoute = require('./routes/favourites');
const toRoute = require('./routes/torecord');
const getjson = require('./routes/getJson');
const getjson_id = require('./routes/getjson_id');

app.use('/posts', postsRoute);
app.use('/favourites', favouritesRoute);
app.use('/to', toRoute);
app.use('/getjson', getjson);
app.use('/getjson_id', getjson_id);

app.listen(3000, () => console.log('Connect Mongodb port: 3000'));

module.exports = app;