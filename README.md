# Driver - web-based system

# npm command
npm install express nodemon
npm install body-parser
npm install mockgoose
npm install chai
npm install kill
npm install lsof
npm install mocha
npm install save
npm install undo
npm install supertest
npm install sudo
npm install chai-http
npm install chai-things
npm install express
npm install mongodb
npm install should
npm install

# Code coverage with Mocha
npm install nyc

# Link Detail
/getjson is for getting data from gov data website
/getjson_id is for getting data by id from gov data website
/favourites is for favourite route management
/to is for route record management
/posts is for vehicle data management